from pathlib import Path
import re
DATASETS=["1","2"]
FORMATS=["bam"]
fa_dir=Path(".")

def get_bams(fa_dir):
    """*fa_dir* is a directory that contains *.bam files."""
    for fa_file in fa_dir.glob("*.bam"):
        fa_match = re.match("(.*).bam", fa_file.name)
        if fa_match:
            (cell_id,) = fa_match.groups()
            yield cell_id

bams=get_bams(fa_dir)
sources=[i+".fa" for i in list(bams)]
results=[i+".pure.fa" for i in list(bams)]
print(list(results))
def pair(wildcards):
    """data* is a directory that contains *.fq/fastq/gz files."""
    prefix=wildcards.sample
    dir="data/"
    suffix='.*_(R1|R2).*(fastq|fq)\.gz$'
    print ("called with", prefix, suffix)
    pattern=prefix+suffix
    files = [f for f in os.listdir(dir) if re.match(pattern, f)]
    myfiles=[dir+files[0],dir+files[1]]
    print(myfiles)
    return myfiles

rule pure:
    input:
        "data/{sample}.bam"
    output:
        "{sample}/{sample}.pure.fq"
    threads: 95
    params:
        hg="/local/databases/index/bowtie/2.3.5.1/hg38",
        module2load="bowtie2/2.3.5.1 samtools/1.10",
        cleaner="/pasteur/zeus/projets/p02/PGP_Work/METAL/metal/fq_cleaner_kp_slurm.sh"
    shell:
        "module load {params.module2load}; mkdir -p {wildcards.sample}/tmp;cd {wildcards.sample};"
        "samtools fastq -1 xx1.fq -2 xx2.fq -n ../data/{wildcards.sample}.bam >tmp/{wildcards.sample}.fq;"
        "{params.cleaner} -f tmp/{wildcards.sample}.fq -q 18 -l 30  -p 70 -x tmp/{wildcards.sample}.noaliens.fq;"
        "bowtie2 -p {threads} -U tmp/{wildcards.sample}.noaliens.fq -x {params.hg}.fa --un {wildcards.sample}.pure.fq >/dev/null;"


rule pure2:
    input:
        samples = pair
    output:
        "{sample}/{sample}.pure.1.fq",
        "{sample}/{sample}.pure.2.fq"
    threads: 95
    params:
        hg="/local/databases/index/bowtie/2.3.5.1/hg38",
        module2load="bowtie2/2.3.5.1 samtools/1.10",
        cleaner="/pasteur/zeus/projets/p02/PGP_Work/METAL/metal/fq_cleaner_kp_slurm.sh"
    shell:
        "echo purity2 ;"
        "module load {params.module2load}; mkdir -p {wildcards.sample}/tmp;cd {wildcards.sample};"
        "echo xxx {input[0]} -r {input[1]} tmp/{wildcards.sample}.noaliens.R1.fq ;"
        "{params.cleaner} -f ../{input[0]} -r ../{input[1]} -q 18 -l 30  -p 70 -x tmp/{wildcards.sample}.noaliens.R1.fq -y tmp/{wildcards.sample}.noaliens.R2.fq;"
        "bowtie2 -p {threads} -1 tmp/{wildcards.sample}.noaliens.R1.fq -2 tmp/{wildcards.sample}.noaliens.R2.fq  -x {params.hg}.fa --un-conc {wildcards.sample}.pure.%.fq >/dev/null;"


rule megahit2:
    input:
        "{sample}/{sample}.pure.1.fq",
        "{sample}/{sample}.pure.2.fq"
    output:
        "{sample}/megahit/final.contigs.fa"
    params:
        module2load="megahit/1.2.9"
    threads: 95
    shell:
        "module load {params.module2load}; cd {wildcards.sample};rm -rf megahit;"
        "megahit -1 {wildcards.sample}.pure.1.fq -2 {wildcards.sample}.pure.2.fq -t {threads} -o megahit"

rule megahit:
    input:
        "{sample}/{sample}.pure.fq"
    output:
        "{sample}/megahit/final.contigs.fa"
    params:
        module2load="megahit/1.2.9"
    threads: 95
    shell:
        "module load {params.module2load}; cd {wildcards.sample};rm -rf megahit;"
        "megahit -r {wildcards.sample}.pure.fq -t {threads} -o megahit"

rule taxo:
    input:
        "{sample}/megahit/final.contigs.fa"
    output:
        "{sample}_2bLCA_lca.tsv"
    threads: 95
    params:
        module2load="MMseqs2/13-45111",
        uniprot="/pasteur/zeus/services/p01/banques-dev/prod/rel/uniprot/uniprot_2021_02/mmseq/13/uniprot_trembl"
    shell:
        "module load {params.module2load};cd {wildcards.sample};mkdir -p mmtmp;"
        "mmseqs easy-taxonomy --threads {threads} ./megahit/final.contigs.fa {params.uniprot} {wildcards.sample}_2bLCA mmtmp;"
        "cp {wildcards.sample}_2bLCA_* .. "
