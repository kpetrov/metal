#!/bin/bash

### Author: Konstantin Petrov and Sophie Creno @HPC2F after MV for VH
### This is metal, the metagenomic pipeline in Maestro HPC environment
## set -e

### METAGENOMIC PIPELINE : From a .bam sequence file or a pair if .fastq files, this pipeline will realize a cleaning, decontamination, classification and get the viral taxonomic content.
#Suited for a SLURM grid parallelization
#This program has the following entries :
#1 - the input file containing the sequences to analyze (mandatory)
#2 - an output name (mandatory)
#3 - the filtering length for Fastq Cleaner (by default = 30)
#4 - the filtering quality treshold (Phred score) for Fastq Cleaner (by default = 20)
#5 - the filtering % of bases meeting the quality threshold for Fastq Cleaner (by default = 80)

#Calling example (single end bam data) : sh pipeline-metageno2-slurm.sh -i sequences.bam -o Run_300516_RDC7 -l 30 -q 18 -p 70
#Calling example (single end data) : sh pipeline-metageno2-slurm.sh -i sequences.fastq -o Run_300516_RDC7 -l 30 -q 18 -p 70
#Calling example (paired end data) : sh pipeline-metageno2-slurm.sh -f sequences_1.fastq -r sequences_2.fastq -o Run_300516_RDC7 -l 30 -q 18 -p 70

source /opt/gensoft2/adm/etc/profile.d/modules.sh
## load modules
module load soap perl Python/2.7.8 openmpi/3.1.4 blast/2.2.26 ptools/0.99d golden taxoptimizer ;
## modif VH add perl dans module load memo pb kraken AK

## DEPENDENCIES:
# [1] KRAKEN2DB:
# - should be in ${SCRIPTDIR}/Kraken2/DB/
# - contains: archaea  bacteria  env_nt  fungi  human  plasmid  protozoa  viral
# - update procedure to follow on https://ccb.jhu.edu/software/kraken2/index.shtml?t=manual#custom-databases

#########################################
########## OPTIONS & VARIABLES ##########
#########################################
SUBMIT1='srun -q bench -p bench';
SUBMIT2='sbatch -q bench -p bench --wait';
SUBMIT3='salloc --partition=common --qos=normal --nice=0 -N 10 -c 1';
MEM1='--mem=20G';
MEMSOAP='--mem=150G';
MEMKRAKEN='--mem=150G';
MEMGETSEQ='--mem=500m';
MEM4='--mem=40000';
CORES=12;
PPL_VERSION="KP";

FQL=30 # Fqcleaner length filter
FQQ=18 # Fqcleaner Phred filter
FQP=70 # Fqcleaner % filter
BEV=10E-5 # BlastN E-value

usage="Usage : $(basename "$0") [-h] [-i input_file] [-o output_name] [(optional) -e blastN_EVal -l -q -p length/phred/% filtering parameters]
	$(basename "$0") [-h] [-f input_file_R1] [-r input_file_R2] [-o output_name] [(optional) -e blastN_EVal -l -q -p length/phred/% filtering parameters]"

while getopts :i:f:r:o:l:q:p:h: option
do
    case $option in
	i) INPUT=$OPTARG
	    iflag=true;
	    if [[ ! -e ${INPUT} ]] ; then echo bailout; fi;; 
	f) PE_INPUT_1=$OPTARG ;
	    if [[ ! -e $PE_INPUT_1 ]]; then echo "err : problem with input file (option -f): '$PE_INPUT_1' does not exist" ; exit ; fi;
	    iflag=true;;
	r) PE_INPUT_2=$OPTARG ;
	    if [[ ! -e $PE_INPUT_2 ]]; then echo "err : problem with input file (option -r): '$PE_INPUT_2' does not exist" ; exit ; fi;
	    iflag=true;;
	o)	OUTPUT=$OPTARG
	    oflag=true;;
	l)	FQL=$OPTARG;;
	q)	FQQ=$OPTARG;;
	p)	FQP=$OPTARG;;
	h)	echo "$usage"
	    exit 1;;
	:)	printf "missing argument for -%s\n" "$OPTARG" >&2
	    echo "$usage" >&2
	    exit 1 ;;
	\?)	printf "illegal option: -%s\n" "$OPTARG" >&2
	    echo "$usage" >&2
	    exit 1 ;;
    esac
done
shift "$((OPTIND-1))"

if ([ -z $INPUT ] && [ -z $oflag ] )  || ([ -z $PE_INPUT_1 ] && [ -z $PE_INPUT_2 ] && [ -z $oflag ])
then
    echo "Exit : Missing mandatory option(s) to run the pipeline" >&2
    echo "$usage";
    exit 1;
fi

DIR=$( dirname "${INPUT}" )
INPUT_NAME=$( basename "${INPUT}" )
SCRIPTDIR=/pasteur/projets/specific/PGP_Work/Scripts
KRAKENDIR=${SCRIPTDIR}/Kraken/BUILD
KRAKEN2DIR=${SCRIPTDIR}/Kraken2/BUILD/
KRAKEN2DB=${SCRIPTDIR}/Kraken2/DB/
NTDB=/pasteur/services/policy01/banques/prod/rel/nt/current/blast2/2.2.26/nt ## NTDB=/pasteur/services/policy01/banques/prod/rel/nt/nt_2018-09-24/blast2/2.2.26/nt 
VRLDB=/pasteur/services/policy01/banques/prod/rel/genbank_release/current/blast2/2.2.26/genbank_release_vrl
UNIPROTDB=/pasteur/services/policy01/banques/prod/rel/uniprot/current/blast2/2.2.26/uniprot

if [[ ! -d  ${DIR}/${OUTPUT} ]]; then
    mkdir ${DIR}/${OUTPUT};
else
    echo  ${DIR}/${OUTPUT} already exists. Continue...  2>&1 
fi;

LOGFILE=`readlink -f ${DIR}`/${OUTPUT}/${OUTPUT}.ppl.log
if [[ -e ${LOGFILE} ]]; then
    echo WARNING log file already exists -- Continue anyway ...  2>&1 | tee -a ${LOGFILE} ;
else
    echo -n "New Analysis started: " 2>&1 | tee  ${LOGFILE}  ;
fi;
echo `date` 2>&1 | tee -a ${LOGFILE} ;
echo "MetaGeno PipeLine Version Number ${PPL_VERSION} " 2>&1 | tee -a ${LOGFILE} ;
echo  | tee -a ${LOGFILE} ;

echo "########## STARTING METAGENOMIC ANALYSIS ##########" | tee -a  ${LOGFILE} ;

cd ${DIR}/${OUTPUT}

## Si on a un fichier .bam (Ion Torrent), on traduit de bam -> fastq
# BAM 2 FASTQ
if [ "${INPUT##*.}" == "bam" ];then
    ln -fs ../${INPUT_NAME} ${OUTPUT}.bam;
    echo "... Step 1 : Bam to Fastq ..." | tee -a  ${LOGFILE} ;
    ${SUBMIT1} ${SCRIPTDIR}/bam2fastq-1.1.0/bam2fastq -o ${OUTPUT}.fastq ${OUTPUT}.bam 2>&1 | tee -a ${LOGFILE}  ;
## si ce n'est pas le cas, on met un lien
else
# cas PE
 if [[ -z $INPUT ]]; then
    ln -fs ../${PE_INPUT_1} ${OUTPUT}_1.fastq;
    ln -fs ../${PE_INPUT_2} ${OUTPUT}_2.fastq ;
 else
# cas SE
   ln -fs ../${INPUT} ${OUTPUT}.fastq
 fi;
fi;

## Nettoyage des donnees: on utilise fqcleaner:
# FASTQCLEANER
echo "... Step 2 : Fastq Cleaner ..." | tee -a  ${LOGFILE}  ;
echo "... Step 2.1 : Trimming and cleaning ..." | tee -a  ${LOGFILE} ;
 OUTPUT_CLEAN=${OUTPUT}.q${FQQ}p${FQP}l${FQL}e${BEV}.CFQD
if [[ ! -z $INPUT ]]; then
    echo "...... Single end cleaning ..." | tee -a  ${LOGFILE} ;
    export TMPDIR=${MYSCRATCH}
    ${SUBMIT1} ${MEMSOAP} ${SCRIPTDIR}/fqCleaner-slurm.sh -f ${OUTPUT}.fastq -q ${FQQ} -l ${FQL} -p ${FQP} -x ${OUTPUT_CLEAN}.fq 2>&1 | tee -a  ${LOGFILE} ;
fi

if [[ ! -z $PE_INPUT_1 && ! -z $PE_INPUT_2 ]]; then
    echo "...... Paired end cleaning ..." | tee -a  ${LOGFILE} ;
    export TMPDIR=${MYSCRATCH}
    ${SUBMIT1} ${MEMSOAP} ${SCRIPTDIR}/fqCleaner-slurm.sh -f ${OUTPUT}_1.fastq -r ${OUTPUT}_2.fastq -q ${FQQ} -l ${FQL} -p ${FQP} -x ${OUTPUT_CLEAN}_1.fq -y ${OUTPUT_CLEAN}_2.fq -z ${OUTPUT_CLEAN}_sgl.fq 2>&1 | tee -a  ${LOGFILE} ; 

    echo "...... Paired end cleanings -> Concatenation ..." | tee -a  ${LOGFILE} ;
    cat ${OUTPUT_CLEAN}_1.fq ${OUTPUT_CLEAN}_2.fq ${OUTPUT_CLEAN}_sgl.fq > ${OUTPUT_CLEAN}.fq; 

fi;

## Dans l'evolution du formattage des donnees, le MiSeq a commence a introduire des espaces, ce qui gene les etapes en aval. On elimine donc les espaces:
echo "... Step 3 : Data (re)formatting ..." | tee -a  ${LOGFILE} ;

if [ $(head -n 1 ${OUTPUT_CLEAN}.fq  | awk -F" " '{print NF}') -gt 1 ]; then has_spaces=1; else has_spaces=0; fi;

echo "... Step 3.1 : Forcing Reformat (in case we have HISEQ formattings)  ..." | tee -a  ${LOGFILE} ;
if [[ $has_spaces -ge 1 ]]; then
    ${SUBMIT1}  perl /pasteur/projets/specific/PGP_Work/Scripts/miseq_reformat_fq.pl ${OUTPUT_CLEAN}.fq ${OUTPUT_CLEAN}.reformat.fq 2>&1 | tee -a  ${LOGFILE} ; 
    mv -f ${OUTPUT_CLEAN}.reformat.fq ${OUTPUT_CLEAN}.fq; 
fi;

# On traduit de fastq -> fasta (pour soap, kraken, etc).
# FASTQ 2 FASTA
echo "... Step 3.2 : Fastq to Fasta ..." | tee -a  ${LOGFILE} ;
if [[ ! -e ${OUTPUT_CLEAN}.fasta ]]; then
    ${SUBMIT1} ${MEMGETSEQ}  perl ${SCRIPTDIR}/fastq2fasta.pl ${OUTPUT_CLEAN}.fq ${OUTPUT_CLEAN}.fasta 2>&1 | tee -a  ${LOGFILE}  ;
fi;

# ON a convenu (cfr reunion PGP) de ne plus utiliser deconseq. On revient donc sur un mapper plus conventionnel, ici soap:
# SOAP (was formerly DECONSEQ)
echo "... Step 4 : Soap decontamination ..." | tee -a  ${LOGFILE} ;
 OUTPUT_DECONT=${OUTPUT_CLEAN}_clean
${SUBMIT1} -n 1 -c ${CORES} ${MEMSOAP} -J Soap soap -p ${CORES} -a ${OUTPUT_CLEAN}.fasta -D /pasteur/services/policy01/banques/prod/rel/hg38/current/soap/2.21/hg38.fa.index -o ${OUTPUT_CLEAN}_cont.aln -u ${OUTPUT_DECONT}.fasta -v 5 -M 4 2>&1 | tee -a  ${LOGFILE}  ;

# KRAKEN2
# voir commentaire en debut pour mise a jour de la base
echo "... Step 5.1 : Kraken2 Classification ..." | tee -a  ${LOGFILE} ;
 OUTPUT_KRAKEN2=${OUTPUT_DECONT}.kraken2

${SUBMIT1} -c ${CORES} ${MEMKRAKEN} -J kraken ${KRAKEN2DIR}/kraken2 --threads ${CORES} -db ${KRAKEN2DB} --unclassified-out ${OUTPUT_KRAKEN2}.unclassified --classified-out ${OUTPUT_KRAKEN2}.classified  --output ${OUTPUT_KRAKEN2} --report ${OUTPUT_KRAKEN2}.report --report-zero-counts ${OUTPUT_DECONT}.fasta  2>&1 | tee -a  ${LOGFILE} ;


## association de la taxonomy aux identifications Kraken, pour plus facilement filtrer selon la categorie:
echo "... Step 5.2 : Get kraken2 taxo/labels ..." | tee -a  ${LOGFILE} ;

 OUTPUT_LABELS=${OUTPUT_KRAKEN2}.taxo

${SUBMIT1}  ${SCRIPTDIR}/from_many_taxids_to_taxo /pasteur/gaia/projets/p01/PGP_Work/Scripts/PgpSlurmBlast2Taxo/taxonomy.db ${OUTPUT_KRAKEN2} ${OUTPUT_LABELS} ${OUTPUT_KRAKEN2}.notaxo  2>&1 | tee -a  ${LOGFILE} ;

# GET SPECIES FASTA SEQUENCES
echo "... Step 6 : Get Virus and Bacteria fasta sequences ..." | tee -a  ${LOGFILE} ;
echo "... Step 6.1 : Get fasta sequences out of kraken2 ..." | tee -a  ${LOGFILE} ;

 OUTPUT_VIRUS=${OUTPUT_LABELS}.Virus ;
 OUTPUT_BACTERIA=${OUTPUT_LABELS}.Bacteria ;
 OUTPUT_FUNGI=${OUTPUT_LABELS}.Fungi ;
 OUTPUT_EUKARYOTA=${OUTPUT_LABELS}.Eukaryota ;
 OUTPUT_OTHER=${OUTPUT_LABELS}.Other ; ##other are classifieds, but no virus-bacteria-fungi-eukaryota
 OUTPUT_UNCLASSIFIED=${OUTPUT_DECONT}_unclassifieds ;

echo " ... ... Bacteria Viruses Fungi ..." | tee -a  ${LOGFILE} ;
${SUBMIT1}  ${MEMGETSEQ}  -J virus ${SCRIPTDIR}/get_seq_from_kraken2_taxo.sh -f ${OUTPUT_DECONT}.fasta -k ${OUTPUT_LABELS} -o ${OUTPUT_LABELS}.Virus.fasta -t 'Virus' ;
${SUBMIT1}  ${MEMGETSEQ}  -J bacteria ${SCRIPTDIR}/get_seq_from_kraken2_taxo.sh -f ${OUTPUT_DECONT}.fasta -k ${OUTPUT_LABELS} -o ${OUTPUT_LABELS}.Bacteria.fasta -t 'Bacteria' ;
${SUBMIT1}  ${MEMGETSEQ}  -J fungi ${SCRIPTDIR}/get_seq_from_kraken2_taxo.sh -f ${OUTPUT_DECONT}.fasta -k ${OUTPUT_LABELS} -o ${OUTPUT_LABELS}.Fungi.fasta -t 'Fungi' ;

echo " ... ... get Eukaryota (but no Fungi) ..." | tee -a  ${LOGFILE} ;
${SUBMIT2}   --wrap="grep '^C' ${OUTPUT_LABELS} | grep -i eukaryota | grep -v -i fungi |  cut -f 2 > ${OUTPUT_LABELS}.Eukaryota.list";
${SUBMIT1}  ${MEMGETSEQ}   -J get_euk python2.7 ${SCRIPTDIR}/get_fasta_from_name_seq.py ${OUTPUT_DECONT}.fasta ${OUTPUT_LABELS}.Eukaryota.list ${OUTPUT_LABELS}.Eukaryota.fasta 2>&1 | tee -a  ${LOGFILE} ;

echo " ... ... get Unclassifieds ..." | tee -a  ${LOGFILE} ;
${SUBMIT1} -J get_unclassifieds sh ${SCRIPTDIR}/kraken2_get_unclassified_fasta.sh ${OUTPUT_DECONT}.fasta ${OUTPUT_KRAKEN2} ;

echo " ... ... get Other (classified, but not virus, not bacteria, not fungi, not eukyota ... "  | tee -a  ${LOGFILE} ;
${SUBMIT2} --wrap="grep '^C' ${OUTPUT_LABELS} | grep -v -i virus | grep -v -i bacteria | grep -v -i fungi | grep -v -i eukaryota | cut -f 2 > ${OUTPUT_LABELS}.Other.list";
${SUBMIT1}  ${MEMGETSEQ}    -J get_other python2.7 ${SCRIPTDIR}/get_fasta_from_name_seq.py ${OUTPUT_DECONT}.fasta ${OUTPUT_LABELS}.Other.list ${OUTPUT_LABELS}.Other.fasta 2>&1 | tee -a  ${LOGFILE} ;

#Ok, so now we got a number of files:
# *.Virus.fasta
# *.Bacteria.fasta
# *.Fungi.fasta
# *.Eukaryota.fasta
# *.Other.fasta
# *_unclassified.fasta

# comptage, superflu, mais utile?
echo "... Step 6.2 : Count Virus and Bacteria fasta sequences ..." | tee -a  ${LOGFILE} ;

for SPECIES in VIRUS BACTERIA FUNGI EUKARYOTA UNCLASSIFIED OTHER; do 
    OUTPUT_SPECIES=OUTPUT_${SPECIES} ;
    export ${SPECIES}nbseq=`grep -c "^>" ${!OUTPUT_SPECIES}.fasta`;
    ${SUBMIT2} --wrap="grep '^>' ${!OUTPUT_SPECIES}.fasta | tr -d '^>' > ${!OUTPUT_SPECIES}.list" ;
    ${SUBMIT1} ${MEMGETSEQ} perl ${SCRIPTDIR}/get_fastq_from_name_seq.pl ${!OUTPUT_SPECIES}.list ${OUTPUT_CLEAN}.fq ${!OUTPUT_SPECIES}.fastq | tee -a ${LOGFILE} ;
done

## Assembly for all items:
echo "... Step 6.3 : Assembly of Virus, Bacteria, ... fasta sequences ..." | tee -a  ${LOGFILE} ;
for SPECIES in VIRUS BACTERIA FUNGI EUKARYOTA UNCLASSIFIED OTHER; do 
	echo ${SPECIES} ...
    OUTPUT_SPECIES=OUTPUT_${SPECIES} ;
    ${SUBMIT1}  ${MEMCLC} -J clc5  -c ${CORES}  perl ${SCRIPTDIR}/clc5_denovo_hpc.pl -q ${!OUTPUT_SPECIES}.fasta  -c ${CORES}   2>&1 | tee -a ${LOGFILE} ;
    cd ${!OUTPUT_SPECIES}-CLC/
## Renaming contigs and unassembled_reads files:
    OUTPUT_CONTIG=${!OUTPUT_SPECIES}.contigs
    OUTPUT_UNASSEMBLED=${!OUTPUT_SPECIES}.unassembled_reads
    mv contigs.fasta ${OUTPUT_CONTIG}.fasta ;
    mv unassembled_reads.fasta ${OUTPUT_UNASSEMBLED}.fasta;
    cd ../ ;
done

echo "... Step 7 : Blast ..."| tee -a  ${LOGFILE}  ;

for SPECIES in VIRUS BACTERIA FUNGI EUKARYOTA UNCLASSIFIED OTHER; do 
    OUTPUT_SPECIES=OUTPUT_${SPECIES};

# BLASTN
    echo "... Step 7.x : ${SPECIES} -- Blast ..." | tee -a  ${LOGFILE} ;

    cd ${!OUTPUT_SPECIES}-CLC;

    OUTPUT_CONTIG=${!OUTPUT_SPECIES}.contigs
    OUTPUT_UNASSEMBLED=${!OUTPUT_SPECIES}.unassembled_reads
    
    BLASTN_DB=${NTDB}; sh ${SCRIPTDIR}/mblastall.sh ${OUTPUT_CONTIG}.fasta ${OUTPUT_CONTIG}.blastn ${BEV} ${BLASTN_DB} blastn
    
    export TMPDIR=${MYSCRATCH}
    ${SUBMIT1} ${MEMGETSEQ} sh /pasteur/projets/specific/PGP_Work/Scripts/repriseEchecBlast.sh -f ${OUTPUT_CONTIG}.fasta -r ${OUTPUT_CONTIG}.blastn -o ${OUTPUT_CONTIG}.noblastn.fasta 2>&1 | tee -a ${LOGFILE} ;   sleep 5;
    
    sh ${SCRIPTDIR}/mblastall.sh ${OUTPUT_CONTIG}.noblastn.fasta ${OUTPUT_CONTIG}.noblastn.blastx 100 ${UNIPROTDB} blastx
    
    module load blast cd-hit
    ${SUBMIT1} cd-hit-est -c 0.99 -d 0 -T 1 -M 0 -i ${OUTPUT_UNASSEMBLED}.fasta -o ${OUTPUT_UNASSEMBLED}.cdhit.fasta ;
    mv -f ${OUTPUT_UNASSEMBLED}.cdhit.fasta ${OUTPUT_UNASSEMBLED}.fasta
    BLASTN_DB=${NTDB}; sh ${SCRIPTDIR}/mblastall.sh ${OUTPUT_UNASSEMBLED}.fasta ${OUTPUT_UNASSEMBLED}.blastn ${BEV} ${BLASTN_DB} blastn
    
    export TMPDIR=${MYSCRATCH}
    ${SUBMIT1} ${MEMGETSEQ} sh /pasteur/projets/specific/PGP_Work/Scripts/repriseEchecBlast.sh -f ${OUTPUT_UNASSEMBLED}.fasta -r ${OUTPUT_UNASSEMBLED}.blastn -o ${OUTPUT_UNASSEMBLED}.noblastn.fasta 2>&1 | tee -a ${LOGFILE} ;     sleep 5;
    
    sh ${SCRIPTDIR}/mblastall.sh ${OUTPUT_UNASSEMBLED}.noblastn.fasta ${OUTPUT_UNASSEMBLED}.noblastn.blastx 100 ${UNIPROTDB} blastx
    
    cd ..
    
done

# Blast best score, on garde le best-hit:
echo "... Step 7.3 : Get blastn best score ..." | tee -a  ${LOGFILE};

for SPECIES in VIRUS BACTERIA FUNGI EUKARYOTA UNCLASSIFIED OTHER; do 
    OUTPUT_SPECIES=OUTPUT_${SPECIES};
    
    echo "... Step 7.3.x : ${SPECIES} -- Get blast best scores ..." | tee -a  ${LOGFILE};

    cd ${!OUTPUT_SPECIES}-CLC;
    
    OUTPUT_CONTIG=${!OUTPUT_SPECIES}.contigs
    OUTPUT_CONTIG_BLASTN_BEST=${OUTPUT_CONTIG}.blastn.best_score
    OUTPUT_UNASSEMBLED=${!OUTPUT_SPECIES}.unassembled_reads
    OUTPUT_UNASSEMBLED_BLASTN_BEST=${OUTPUT_UNASSEMBLED}.blastn.best_score
    ${SUBMIT1} -J score1 ${MEMGETSEQ} perl ${SCRIPTDIR}/best_score_v4.pl -i ${OUTPUT_CONTIG}.blastn -o ${OUTPUT_CONTIG_BLASTN_BEST} 2>&1 | tee -a  ${LOGFILE} ; sleep 5;
    ${SUBMIT1} ${MEMGETSEQ} -J score2 perl ${SCRIPTDIR}/best_score_v4.pl -i ${OUTPUT_CONTIG}.noblastn.blastx -o ${OUTPUT_CONTIG}.noblastn.blastx.best_score 2>&1 | tee -a  ${LOGFILE} ; sleep 5;
    ${SUBMIT1} ${MEMGETSEQ} -J score3 perl ${SCRIPTDIR}/best_score_v4.pl -i ${OUTPUT_UNASSEMBLED}.blastn -o ${OUTPUT_UNASSEMBLED_BLASTN_BEST} 2>&1 | tee -a  ${LOGFILE} ; sleep 5;
    ${SUBMIT1} ${MEMGETSEQ} -J score4 perl ${SCRIPTDIR}/best_score_v4.pl -i ${OUTPUT_UNASSEMBLED}.noblastn.blastx -o ${OUTPUT_UNASSEMBLED}.noblastn.blastx.best_score 2>&1 | tee -a  ${LOGFILE} ; sleep 5;
    
    cd ..
    
done

# Association de la taxo aux resultat blast:
echo "... Step 7.4 : Get blast[nx] TAXO ..." | tee -a  ${LOGFILE} ;

for SPECIES in VIRUS BACTERIA FUNGI EUKARYOTA UNCLASSIFIED OTHER; do 
    OUTPUT_SPECIES=OUTPUT_${SPECIES};
    
    echo "... Step 7.4.x : ${SPECIES} -- Get blast[nx] TAXO ..." | tee -a  ${LOGFILE} ;
    
    cd ${!OUTPUT_SPECIES}-CLC;
    
    OUTPUT_CONTIG=${!OUTPUT_SPECIES}.contigs
    OUTPUT_CONTIG_BLASTN_BEST=${OUTPUT_CONTIG}.blastn.best_score
    OUTPUT_UNASSEMBLED=${!OUTPUT_SPECIES}.unassembled_reads
    OUTPUT_UNASSEMBLED_BLASTN_BEST=${OUTPUT_UNASSEMBLED}.blastn.best_score
   
    ${SUBMIT3}  mtaxoptimizer -n 10 -i ${OUTPUT_CONTIG_BLASTN_BEST} -o ${OUTPUT_CONTIG_BLASTN_BEST}.taxo -f ${OUTPUT_CONTIG_BLASTN_BEST}.notaxo 

    OUTPUT_CONTIG_BLASTX_BEST=${OUTPUT_CONTIG}.noblastn.blastx.best_score
    OUTPUT_UNASSEMBLED_BLASTX_BEST=${OUTPUT_UNASSEMBLED}.noblastn.blastx.best_score

    ${SUBMIT3} mtaxoptimizer -n 10 -i ${OUTPUT_CONTIG_BLASTX_BEST} -o ${OUTPUT_CONTIG_BLASTX_BEST}.taxo -f ${OUTPUT_CONTIG_BLASTX_BEST}.notaxo
    
    ${SUBMIT3} mtaxoptimizer -n 10 -i ${OUTPUT_UNASSEMBLED_BLASTN_BEST} -o ${OUTPUT_UNASSEMBLED_BLASTN_BEST}.taxo  -f ${OUTPUT_UNASSEMBLED_BLASTN_BEST}.notaxo
    
    ${SUBMIT3} mtaxoptimizer -n 10 -i ${OUTPUT_UNASSEMBLED_BLASTX_BEST} -o ${OUTPUT_UNASSEMBLED_BLASTX_BEST}.taxo  -f ${OUTPUT_UNASSEMBLED_BLASTX_BEST}.notaxo
    
    cd ..
done


for SPECIES in VIRUS BACTERIA FUNGI EUKARYOTA UNCLASSIFIED OTHER; do 
    OUTPUT_SPECIES=OUTPUT_${SPECIES};
    
# GET SPECIES FINAL TAXO
    echo "... Step 8 : Creation of tabulated output files ..." | tee -a  ${LOGFILE} ;
    
    LINE="Query	Hit	%Homology	Alignment_Length	MisMatch	Gap	Query_start	Query_end	Hit_start	Hit_end	E-value	Bitscore	ID	Taxo";
    
    cd ${!OUTPUT_SPECIES}-CLC;
    
    LINE_CONTIG="Query	Hit	%Homology	Alignment_Length	MisMatch	Gap	Query_start	Query_end	Hit_start	Hit_end	E-value Bitscore	ID	Taxo	Nb_reads";
    
    OUTPUT_CONTIG=${!OUTPUT_SPECIES}.contigs
    OUTPUT_UNASSEMBLED=${!OUTPUT_SPECIES}.unassembled_reads
    
    OUTPUT_CONTIG_BEST=${OUTPUT_CONTIG}.blastn.best_score
    
    for SPECIES2 in VIRUS BACTERIA FUNGI EUKARYOTA OTHER; do 
	
        OUTPUT_FILE_CONTIG=../${OUTPUT}.blastn.${SPECIES2,,}.xls
	
	if ([ ${SPECIES2} == "VIRUS" ] || [ ${SPECIES2} == "BACTERIA" ] || [ ${SPECIES2} == "FUNGI" ]); then
	    grep -i ${SPECIES2} ${OUTPUT_CONTIG_BEST}.taxo > ${OUTPUT_FILE_CONTIG}.tmp
	elif ([ ${SPECIES2} == "EUKARYOTA" ]); then
	    grep -i ${SPECIES2} ${OUTPUT_CONTIG_BEST}.taxo | grep -v -i fungi > ${OUTPUT_FILE_CONTIG}.tmp;
	elif ([ ${SPECIES2} == "OTHER" ]); then
	    grep -v -i virus  ${OUTPUT_CONTIG_BEST}.taxo | grep -v -i bacteria | grep -v -i fungi | grep -v -i eukaryota  > ${OUTPUT_FILE_CONTIG}.tmp;
	fi;
	
	if ([ -s ${OUTPUT_FILE_CONTIG}.tmp ]); then
	    if ([ ! -e ${OUTPUT_FILE_CONTIG} ]); then
		echo "${LINE_CONTIG}" > ${OUTPUT_FILE_CONTIG};
	    fi;
	    cat ${OUTPUT_FILE_CONTIG}.tmp >> ${OUTPUT_FILE_CONTIG};
	fi;
	rm -f ${OUTPUT_FILE_CONTIG}.tmp;
	
    done
    
    OUTPUT_UNASSEMBLED_BEST=${OUTPUT_UNASSEMBLED}.blastn.best_score
    
    for SPECIES2 in VIRUS BACTERIA FUNGI EUKARYOTA OTHER; do 
	
        OUTPUT_FILE_UNASSEMBLED=../${OUTPUT}.blastn.${SPECIES2,,}.xls
	
	if ([ ${SPECIES2} == "VIRUS" ] || [ ${SPECIES2} == "BACTERIA" ] || [ ${SPECIES2} == "FUNGI" ]); then
	    grep -i ${SPECIES2} ${OUTPUT_UNASSEMBLED_BEST}.taxo > ${OUTPUT_FILE_UNASSEMBLED}.tmp;
	elif ([ ${SPECIES2} == "EUKARYOTA" ]); then
	    grep -i ${SPECIES2} ${OUTPUT_UNASSEMBLED_BEST}.taxo | grep -v -i fungi > ${OUTPUT_FILE_UNASSEMBLED}.tmp;
	elif ([ ${SPECIES2} == "OTHER" ]); then
	    grep -v -i virus  ${OUTPUT_UNASSEMBLED_BEST}.taxo | grep -v -i bacteria | grep -v -i fungi | grep -v -i eukaryota  > ${OUTPUT_FILE_UNASSEMBLED}.tmp;
	fi;
	
	if ([ -s  ${OUTPUT_FILE_UNASSEMBLED}.tmp ]); then
	    if ([ ! -e ${OUTPUT_FILE_UNASSEMBLED} ]); then
		echo "${LINE}" > ${OUTPUT_FILE_UNASSEMBLED};
	    fi;
	    cat   ${OUTPUT_FILE_UNASSEMBLED}.tmp >>   ${OUTPUT_FILE_UNASSEMBLED};
	fi;
	rm -f   ${OUTPUT_FILE_UNASSEMBLED}.tmp;
	
    done
    
    OUTPUT_CONTIG_BEST=${OUTPUT_CONTIG}.noblastn.blastx.best_score
    
    if ([ -e ${OUTPUT_CONTIG_BEST}.taxo ]); then
	for SPECIES2 in VIRUS BACTERIA FUNGI EUKARYOTA OTHER; do 
	    
	    OUTPUT_FILE_CONTIG=../${OUTPUT}.blastx.${SPECIES2,,}.xls
	    
	    if ([ ${SPECIES2} == "VIRUS" ] || [ ${SPECIES2} == "BACTERIA" ] || [ ${SPECIES2} == "FUNGI" ]); then
		grep -i ${SPECIES2} ${OUTPUT_CONTIG_BEST}.taxo > ${OUTPUT_FILE_CONTIG}.tmp; 
	    elif ([ ${SPECIES2} == "EUKARYOTA" ]); then
		grep -i ${SPECIES2} ${OUTPUT_CONTIG_BEST}.taxo | grep -v -i fungi > ${OUTPUT_FILE_CONTIG}.tmp;
	    elif ([ ${SPECIES2} == "OTHER" ]); then
		grep -v -i virus  ${OUTPUT_CONTIG_BEST}.taxo | grep -v -i bacteria | grep -v -i fungi | grep -v -i eukaryota  > ${OUTPUT_FILE_CONTIG}.tmp ;
	    fi;
	    
	    if ([ -s  ${OUTPUT_FILE_CONTIG}.tmp ]); then
		if ([ ! -e ${OUTPUT_FILE_CONTIG} ]); then
		    echo "${LINE_CONTIG}" > ${OUTPUT_FILE_CONTIG};
		fi;
		cat ${OUTPUT_FILE_CONTIG}.tmp >> ${OUTPUT_FILE_CONTIG}; 
	    fi;
	    rm -f ${OUTPUT_FILE_CONTIG}.tmp;
	    
	done
    fi;
    
    OUTPUT_UNASSEMBLED_BEST=${OUTPUT_UNASSEMBLED}.noblastn.blastx.best_score
    
    if ([ -e ${OUTPUT_UNASSEMBLED_BEST}.taxo ]); then
	for SPECIES2 in VIRUS BACTERIA FUNGI EUKARYOTA OTHER; do 
	    
	    OUTPUT_FILE_UNASSEMBLED=../${OUTPUT}.blastx.${SPECIES2,,}.xls
	    
	    if ([ ${SPECIES2} == "VIRUS" ] || [ ${SPECIES2} == "BACTERIA" ] || [ ${SPECIES2} == "FUNGI" ]); then
		grep -i ${SPECIES2} ${OUTPUT_UNASSEMBLED_BEST}.taxo > ${OUTPUT_FILE_UNASSEMBLED}.tmp;
	    elif ([ ${SPECIES2} == "EUKARYOTA" ]); then
		grep -i ${SPECIES2} ${OUTPUT_UNASSEMBLED_BEST}.taxo | grep -v -i fungi > ${OUTPUT_FILE_UNASSEMBLED}.tmp ;
	    elif ([ ${SPECIES2} == "OTHER" ]); then
		grep -v -i virus  ${OUTPUT_UNASSEMBLED_BEST}.taxo | grep -v -i bacteria | grep -v -i fungi | grep -v -i eukaryota  > ${OUTPUT_FILE_UNASSEMBLED}.tmp ;
	    fi;
	    
	    if ([ -s  ${OUTPUT_FILE_UNASSEMBLED}.tmp ]); then
		if ([ ! -e ${OUTPUT_FILE_UNASSEMBLED} ]); then
		    echo "${LINE}" > ${OUTPUT_FILE_UNASSEMBLED};
		fi;
		cat ${OUTPUT_FILE_UNASSEMBLED}.tmp >> ${OUTPUT_FILE_UNASSEMBLED};
	    fi;
	    rm -f ${OUTPUT_FILE_UNASSEMBLED}.tmp;
	done
    fi;
    cd ..
    
done;

echo "... Step 9: Creation of Krona HTML files ... " | tee -a  ${LOGFILE};

for i in *.xls; do 
    ${SUBMIT1} sh  ${SCRIPTDIR}/taxo2krona.sh $i; 
done


echo "########## END OF METAGENOMIC ANALYSIS ##########" | tee -a  ${LOGFILE};
echo -n "End of Analysis : " 2>&1 | tee -a  ${LOGFILE} ;




